"use strict"

for (var i = users.length - 1; i>=0 ; i--) {
    var user = users[i];
    var userKeys = Object.keys(user);
    var userKeysCount = userKeys.length;

    users[i] = [];

    for(var j = 0; j < userKeysCount; j++) {
        users[i].push(user[userKeys[j]]);
    }
}

function fnFormatDetails ( oTable, nTr )
{
    var aData = oTable.fnGetData( nTr );
    var sOut = '';
    for (var i = 0; i < aData.length; i++ ) {
        if (i === aData.length - 1) {
            sOut += userKeys[i] + ": " + aData[i]
        } else {
            sOut += userKeys[i] + ": " + aData[i] + ", "
        }
    }

    return sOut;
}

$(document).ready(function() {

    var nCloneTh = document.createElement( 'th' );
    var nCloneTd = document.createElement( 'td' );

    $('#table thead tr').each( function () {
        this.insertBefore( nCloneTh, this.childNodes[0] );
    } );

    $('#table tbody tr').each( function () {
        this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
    } );

    var oTable = $('#table').dataTable({
        "aaData": users,
        "aoColumns": [
            { "sTitle": "Firstname" },
            { "sTitle": "Lastname" },
            { "sTitle": "Birthday" }
        ]
    });
    $('#table tbody').on('click', 'tr', function () {
        var nTr = $(this)[0];
        if ( oTable.fnIsOpen(nTr) ) {
            oTable.fnClose( nTr );
        } else {
            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
        }
    } );

    if (screen.width < 800 || screen.height < 600) {
        var bVis = oTable.fnSettings().aoColumns[2].bVisible;
        oTable.fnSetColumnVis( 2, bVis ? false : true );
    }
} );